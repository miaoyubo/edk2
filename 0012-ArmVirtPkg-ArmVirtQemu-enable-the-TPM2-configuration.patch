From c3b182fe9189137280a5397426cc08b1110aac39 Mon Sep 17 00:00:00 2001
From: Ard Biesheuvel <ard.biesheuvel@linaro.org>
Date: Wed, 26 Feb 2020 20:05:13 +0100
Subject: [PATCH 12/13] ArmVirtPkg/ArmVirtQemu: enable the TPM2 configuration
 module

Enable the DXE phase component that publishes the HII pages and
associated logic to enable TPM2 parameters to be configured by
the user via the setup menu.

This patch ports (parts of) the following commits to ArmVirtQemu:

- 3103389043bd ("OvmfPkg: Add TCG2 Configuration menu to the Device
                Manager menu", 2019-02-11)

- cf3ad972a210 ("OvmfPkg: reorganize TPM2 support in DSC/FDF files",
                2020-01-09)

- f55477fe2d62 ("OvmfPkg: use HII type PCDs for TPM2 config related
                variables", 2020-01-09)

Ref: https://bugzilla.tianocore.org/show_bug.cgi?id=2560
Signed-off-by: Ard Biesheuvel <ard.biesheuvel@linaro.org>
Reviewed-by: Laszlo Ersek <lersek@redhat.com>
Signed-off-by: jiangfangjie <jiangfangjie@huawei.com>
---
 ArmVirtPkg/ArmVirtQemu.dsc           | 9 +++++++++
 ArmVirtPkg/ArmVirtQemuFvMain.fdf.inc | 3 +++
 2 files changed, 12 insertions(+)

diff --git a/ArmVirtPkg/ArmVirtQemu.dsc b/ArmVirtPkg/ArmVirtQemu.dsc
index 291210a..93b982a 100644
--- a/ArmVirtPkg/ArmVirtQemu.dsc
+++ b/ArmVirtPkg/ArmVirtQemu.dsc
@@ -30,6 +30,7 @@
   DEFINE TTY_TERMINAL            = FALSE
   DEFINE SECURE_BOOT_ENABLE      = FALSE
   DEFINE TPM2_ENABLE             = FALSE
+  DEFINE TPM2_CONFIG_ENABLE      = FALSE
 
   #
   # Network definition
@@ -270,6 +271,11 @@
 [PcdsDynamicHii]
   gArmVirtTokenSpaceGuid.PcdForceNoAcpi|L"ForceNoAcpi"|gArmVirtVariableGuid|0x0|FALSE|NV,BS
 
+!if $(TPM2_CONFIG_ENABLE) == TRUE
+  gEfiSecurityPkgTokenSpaceGuid.PcdTcgPhysicalPresenceInterfaceVer|L"TCG2_VERSION"|gTcg2ConfigFormSetGuid|0x0|"1.3"|NV,BS
+  gEfiSecurityPkgTokenSpaceGuid.PcdTpm2AcpiTableRev|L"TCG2_VERSION"|gTcg2ConfigFormSetGuid|0x8|3|NV,BS
+!endif
+
 ################################################################################
 #
 # Components Section - list of all EDK II Modules needed by this Platform
@@ -492,6 +498,9 @@
       NULL|SecurityPkg/Library/HashInstanceLibSha512/HashInstanceLibSha512.inf
       NULL|SecurityPkg/Library/HashInstanceLibSm3/HashInstanceLibSm3.inf
   }
+!if $(TPM2_CONFIG_ENABLE) == TRUE
+  SecurityPkg/Tcg/Tcg2Config/Tcg2ConfigDxe.inf
+!endif
 !endif
 
   #
diff --git a/ArmVirtPkg/ArmVirtQemuFvMain.fdf.inc b/ArmVirtPkg/ArmVirtQemuFvMain.fdf.inc
index 047e99c..2fa69ce 100644
--- a/ArmVirtPkg/ArmVirtQemuFvMain.fdf.inc
+++ b/ArmVirtPkg/ArmVirtQemuFvMain.fdf.inc
@@ -178,6 +178,9 @@ READ_LOCK_STATUS   = TRUE
   #
 !if $(TPM2_ENABLE) == TRUE
   INF SecurityPkg/Tcg/Tcg2Dxe/Tcg2Dxe.inf
+!if $(TPM2_CONFIG_ENABLE) == TRUE
+  INF SecurityPkg/Tcg/Tcg2Config/Tcg2ConfigDxe.inf
+!endif
 !endif
 
   #
-- 
2.18.2

